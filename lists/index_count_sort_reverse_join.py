# Index
# Returns the index of the specified element in the list
fruits = ["banana", "pear", "apple", "orange", "guava", "mango"]

fruits.index("pear") # 1
fruits.index("guava") # 4

# Can specify start and end
names = ["Lucia", "Lucia", "Emelie", "Lina", "Lucia", "Dag", "Dag", "Bjoern", "Vivi"]

names.index("Lucia") # 0 (gives first place it finds Lucia)
names.index("Lucia", 1) # 1 (find Lucia, after the index of 1 inclusively)
names.index("Lucia", 2) # 4

names.index("Dag", 6, 8) # 6 (looks for Dag between the indicies 6-8)

# Count
# Returns the number of times x appears in the list
colors = ["red", "green", "blue", "orange", "blue", "green", "red", "orange"]

colors.count("green") # 2
colors.count("purple") # 0 (purple isn't in list)
colors.count("blue") # 2

# Sort
# Sort the elements of the list (in-place)
letters = ["e", "c", "a", "b", "d"]

letters.sort()

print(letters) # ["a", "b", "c", "d", "e"]

# Reverse
# Reverse the elements of the list (in-place)
numbers = [1, 2, 3, 4]

numbers.reverse()

print(numbers) # [4, 3, 2, 1]

# Join
# Technically a string method that takes an iterable argument
# Concatenates (combines) a copy of the base string between each item of the iterable
# Returns a new string
# Can be used to make sentences out of a list of words by joining on a space
words = ["Coding", "is", "fun!"]

" ".join(words) # Coding is fun!

# Another example
name = ["Mr", "Steele"]

". ".join(name) # Mr. Steele

