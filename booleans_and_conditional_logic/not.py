print('Please input your age:')
age = int(input())
# age 2-8: $2 ticket
# age 65+: $5 ticket
# everyone else: $10 ticket

if not ((age >=2 and age <=8) or age >=65):
	print('Greetings non-child or senior citizen, your ticket will cost $10')
else:
	print('Greetings child or senior citizen, your ticket will cost either $2 or $5 \ndepending on whether you are a child or senior citizen because this is \nterrible code.')
