# ask for age
age = input("How old are you: ")

if age: # empty strings are falsy, so this will be triggered if the string is empty
	age = int(age)
	if age >= 21:
		print("Alright, come on in. Don't get too drunk.")
		# 21+ you can drink
	elif age >= 18:
		print("Alright, come on in. You have to wear this wristband, and no drinking!")
		# 18-21 have to wear wristband
	else:
		print("Sorry kid. Youre to young.")
		# otherwise you're too young, sorry
else:
	print("Are you just going to stand there, or are you going to tell me how old you are?")