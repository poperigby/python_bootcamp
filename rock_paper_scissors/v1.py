# welcome message
print("+-------------ROCK, PAPER, SCISSORS FOR TWO-------------+")
print("|                                                       |")
print("|  Hello, and welcome to Rock, Paper, Scissors for Two. |")
print("|  Grab a friend and let's begin!                       |")
print("|                                                       |")
# print out rules
print("+-----------------------THE RULES-----------------------+")
print("|                                                       |")
print("|  * Rock wins against scissors                         |")
print("|  * Scissors win against paper                         |")
print("|  * Paper wins against rock                            |")
print("|                                                       |")
print("+-------------------------------------------------------+")

player_1_choice = input("Player 1, make your choice: ")
if not player_1_choice:
	print("You have to make a choice!")
elif not (player_1_choice == "rock" or player_1_choice == "paper" or  player_1_choice == "scissors"):
	# check if choice is rock, paper, or scissors
	print("Your choice has to either be 'rock', 'paper', or 'scissors'")
else:
	# if no player 1 errors
	player_2_choice = input("Player 2, make your choice: ")
	if not player_2_choice:
		print("You have to make a choice!")
	elif not (player_2_choice == "rock" or player_2_choice == "paper" or  player_2_choice == "scissors"):
		# check if choice is rock, paper, or scissors
		print("Your choice has to either be 'rock', 'paper', or 'scissors'")
	#if no player 2 errors
	else:
		# player 1 logic
		if player_1_choice == "rock" and player_2_choice == "scissors":
			print("Player 1 wins!")
		elif player_1_choice == "scissors" and player_2_choice == "paper":
			print("Player 1 wins!")
		elif player_1_choice == "paper" and player_2_choice == "rock":
			print("Player 1 wins!")
		# player 2 logic
		elif player_2_choice == "rock" and player_1_choice == "scissors":
			print("Player 2 wins!")
		elif player_2_choice == "scissors" and player_1_choice == "paper":
			print("Player 2 wins!")
		elif player_2_choice == "paper" and player_1_choice == "rock":
			print("Player 2 wins!")
		else:
			print("Tie!")