# import random module
from random import randint

#welcome message
print("+-------------ROCK, PAPER, SCISSORS-------------+")
print("|                                               |")
print("|  Hello, and welcome to Rock, Paper, Scissors  |")
print("|  - Rage Against the Machine Edition           |")
print("|                                               |")
print("|  Play against a super friendly robot, just    |")
print("|  make sure you're friendly too. You don't     |")
print("|  want any angry robots to seek vengeance for  |")
print("|  your crimes against their kind, in the       |")
print("|  distant future.                              |")
# rules
print("+-------------------THE RULES-------------------+")
print("|                                               |")
print("|  * Rock wins against scissors                 |")
print("|  * Scissors win against paper                 |")
print("|  * Paper wins against rock                    |")
print("|                                               |")
# print("+-----------------------------------------------+")

human_wins = 0
computer_wins = 0
winning_score = 2

while human_wins < winning_score and computer_wins < winning_score:
	print("+-----------------------+-----------------------+")
	print(f"|        Human: {human_wins}       |      Computer: {computer_wins}      |")    
	print("+-----------------------+-----------------------+")

	# determine computer's choice
	random_num = randint(1, 3)
	if random_num == 1:
		computer_choice = "ROCK"
	elif random_num == 2:
			computer_choice = "PAPER"
	elif random_num == 3:
		computer_choice = "SCISSORS"

	# ask for human input
	human_choice = input("Human, make your choice: ")

	# check if the human wants to quit the game
	if human_choice.lower() == "quit" or human_choice.lower() == "q":
		print("Quitting. Goodbye!")
		break

	# check if human choice is valid
	if not human_choice:
		print("You have to make a choice!")
	elif not (human_choice.lower() == "rock" or human_choice.lower() == "paper" or  human_choice.lower() == "scissors"):
		# check if choice is rock, paper, or scissors
		print("Your choice has to either be rock, paper, or \nscissors.")
	else:
		# print computer's choice
		print("The computer's choice was: " + computer_choice)
		# determine winner
		if human_choice.lower() == computer_choice.lower():
			    print("This round is a tie!")
		elif human_choice.lower() == "rock" and computer_choice.lower() == "scissors":
		    print("The human won this round!")
		    human_wins += 1
		elif human_choice.lower() == "paper" and computer_choice.lower() == "rock":
		    print("The human won this round!")
		    human_wins += 1
		elif human_choice.lower() == "scissors" and computer_choice.lower() == "paper":
		    print("The human won this round!")
		    human_wins += 1
		else:
		    print("The computer wins this round!")
		    computer_wins += 1

if human_wins > computer_wins:
	print("Congratulations, human!\nBetter luck next time, computer!")
elif human_wins	== computer_wins:
	pass # handles when the user quits after they just tied
elif computer_wins > human_wins:
	print("Congratulations, computer!\nBetter luck next time, human!")

print("+-----------------------------------------------+")
print("|                  FINAL SCORE                  |")
print("+-----------------------+-----------------------+")
print(f"|        Human: {human_wins}       |      Computer: {computer_wins}      |")    
print("+-----------------------+-----------------------+")
	