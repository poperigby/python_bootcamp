print('How many kilometers did you hike today?')
kilometers = input() # takes input and assigns it to 'kilometers'
miles = float(kilometers) / 1.60934 # converts 'kilometers' to a float and then divides that float by 1.60934
miles = round(miles, 2) # rounds miles to 2 decimal places
print (f'Your {kilometers}km hike was {miles}mi.')